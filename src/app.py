from flask import Flask, jsonify, request
from flask_restful import Resource, Api, reqparse
from sqlalchemy import create_engine
from json import dumps
from PersonasDao import Persona
from ClientesDao import Cliente
from EmpleadosDao import Empleado
persona = Persona()
cliente = Cliente()
empleado = Empleado()
app = Flask(__name__)
@app.route('/' , methods = ['GET'])
def listar():
  return jsonify({"mensaje":"Bienvenido a Flask"})

@app.route('/personas/listar', methods=['GET'])
def personas():
    try:
        rows = persona.getAllPersonas()
        respuesta = jsonify(rows)
        respuesta.status_code = 200
        return respuesta
    except Exception as e:
        print(e)

@app.route('/personas/buscarid/<int:idper>')
def buscarID(idper):
    try:
       persona.idpersona = idper
       row = persona.getPersonaID()
       resp =jsonify(row)
       resp.status_code = 200
       return resp
    except Exception as e:
		    print(e)  

@app.route('/personas/add', methods=['POST'])
def addPerson():
    try:
        _json = request.json
        persona.nombres=_json['nombres']
        persona.apellidos=_json['apellidos']
        persona.edad=_json['edad']
        persona.e_civil=_json['e_civil']
        persona.birthday=_json['birthday']
        if request.method=='POST':
            resp=persona.addPersonas()
            resp=jsonify('PERSONA AGREGADA')
            resp.status_code=200
        return resp
    except Exception as e:
        print(e)

@app.route('/personas/upd', methods=['PUT'])   
def updPerson(): 
    try:
        _json = request.json
        persona.nombres=_json['nombres']
        persona.apellidos=_json['apellidos']
        persona.edad=_json['edad']
        persona.e_civil=_json['e_civil']
        persona.birthday=_json['birthday'] 
        persona.idpersona=_json['idper']   
        if request.method == 'PUT':
            resp = persona.updatePersona()
            resp = jsonify('Persona Modificada')
            resp.status_code=200
            return resp
    except Exception as e:
        print(e)

@app.route('/personas/delete/<int:idper>', methods=['GET'])  
def deletePerson(idper):
    try:
        persona.idpersona=idper
        resp=persona.deletePersona()
        resp=jsonify('Persona Eliminada')
        resp.status_code=200
        return resp
    except Exception as e:
        print(e)  

@app.route('/clientes/listar', methods=['GET'])
def clientes():
    try:
        rows = cliente.getAllClientes()
        respuesta = jsonify(rows)
        respuesta.status_code = 200
        return respuesta
    except Exception as e:
        print(e)               

@app.route('/clientes/buscarid/<int:idcli>')
def buscarClienteID(idcli):
    try:
       cliente.idcliente = idcli
       row = cliente.getClienteID()
       resp =jsonify(row)
       resp.status_code = 200
       return resp
    except Exception as e:
		    print(e)  

@app.route('/clientes/add', methods=['POST'])
def addClient():
    try:
        _json = request.json
        cliente.direccion=_json['direccion']
        cliente.dni=_json['dni']
        cliente.idpersona=_json['idpersona']
        if request.method=='POST':
            resp=cliente.addClientes()
            resp=jsonify('CLIENTE AGREGADO')
            resp.status_code=200
        return resp
    except Exception as e:
        print(e)

@app.route('/clientes/upd', methods=['PUT'])   
def updCliente(): 
    try:
        _json = request.json
        cliente.direccion=_json['direccion']
        cliente.dni=_json['dni']
        cliente.idcliente=_json['idcli'] 
        if request.method == 'PUT':
            resp = cliente.updateClientes()
            resp = jsonify('Cliente Modificado')
            resp.status_code=200
            return resp
    except Exception as e:
        print(e)     

@app.route('/clientes/delete/<int:idcli>', methods=['GET'])  
def deleteCliente(idcli):
    try:
        cliente.idcliente=idcli
        resp=cliente.deleteCliente()
        resp=jsonify('Cliente Eliminado')
        resp.status_code=200
        return resp
    except Exception as e:
        print(e)           

@app.route('/empleados/listar', methods=['GET'])
def empleados():
    try:
        rows = empleado.getAllEmpleados()
        respuesta = jsonify(rows)
        respuesta.status_code = 200
        return respuesta
    except Exception as e:
        print(e)

@app.route('/empleados/buscarid/<int:idemp>')
def buscarEmpleadoID(idemp):
    try:
       empleado.idempleado = idemp
       row = empleado.getEmpleadoID()
       resp =jsonify(row)
       resp.status_code = 200
       return resp
    except Exception as e:
		    print(e)          

@app.route('/empleados/add', methods=['POST'])
def addEmpleado():
    try:
        _json = request.json
        empleado.usuario=_json['usuario']
        empleado.password=_json['password']
        empleado.idpersona=_json['idpersona']
        if request.method=='POST':
            resp=empleado.addEmpleados()
            resp=jsonify('EMPLEADO AGREGADO')
            resp.status_code=200
        return resp
    except Exception as e:
        print(e)

@app.route('/empleados/upd', methods=['PUT'])   
def updEmpleado(): 
    try:
        _json = request.json
        empleado.usuario=_json['usuario']
        empleado.password=_json['password']
        empleado.idempleado=_json['idemp'] 
        if request.method == 'PUT':
            resp = empleado.updateEmpleados()
            resp = jsonify('EMPLEADO Modificado')
            resp.status_code=200
            return resp
    except Exception as e:
        print(e)     

@app.route('/empleados/delete/<int:idemp>', methods=['GET'])  
def deleteEmpleado(idemp):
    try:
        empleado.idempleado=idemp
        resp=empleado.deleteEmpleado()
        resp=jsonify('EMPLEADO Eliminado')
        resp.status_code=200
        return resp
    except Exception as e:
        print(e) 

if __name__  == "__main__":
      app.run(host="localhost", port=5000, debug=True)       