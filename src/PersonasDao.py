import pymysql
from conexion import Conexion
from sqlalchemy import create_engine
from json import dumps
cx = Conexion()

class Persona:
    idpersona = 0
    nombres = ""
    apellidos = ""
    edad = ""
    e_civil = ""
    birthday = ""

    def getAllPersonas(self):
        try:
            conexion = cx.conecta()
            cursor = conexion.cursor(pymysql.cursors.DictCursor)
            cursor.callproc('getPersonas')
            rows = cursor.fetchall()
            return rows
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conexion.close()    

    def getPersonaID(self):
        try:
            conexion = cx.conecta()
            cursor = conexion.cursor(pymysql.cursors.DictCursor)
            cursor.callproc('getPersonasId',[self.idpersona])
            rows=cursor.fetchall()
            return rows
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conexion.close() 

    def addPersonas(self):
            nombres = self.nombres
            apellidos = self.apellidos
            edad = self.edad
            e_civil = self.e_civil
            birthday = self.birthday
            data = [nombres,apellidos,edad,e_civil,birthday]
            try:
                conexion=cx.conecta()
                cursor=conexion.cursor(pymysql.cursors.DictCursor)
                cursor.callproc("addPersonas",data)
                conexion.commit()
                return 1
            except Exception as e:
                print(e)
            finally:
                cursor.close()
                conexion.close()  

    def updatePersona(self):
        try:
            idper = self.idpersona
            nombres = self.nombres
            apellidos = self.apellidos
            edad = self.edad
            e_civil = self.e_civil
            birthday = self.birthday
            data = [idper,nombres,apellidos,edad,e_civil,birthday]  
            conexion = cx.conecta()
            cursor=conexion.cursor(pymysql.cursors.DictCursor)
            cursor.callproc("updPersonas",data)
            conexion.commit()
            return 1
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conexion.close() 

    def deletePersona(self):
        try:
            conexion=cx.conecta()
            cursor=conexion.cursor(pymysql.cursors.DictCursor)
            cursor.callproc('deletePersonas',[self.idpersona])
            conexion.commit()
            return 1
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conexion.close()                             
