import pymysql
from conexion import Conexion
from sqlalchemy import create_engine
from json import dumps
cx = Conexion()

class Empleado:
    idempleado = 0
    usuario = ""
    password = ""
    idpersona = 0
    nombres = ""
    apellidos = ""

    def getAllEmpleados(self):
        try:
            conexion = cx.conecta()
            cursor = conexion.cursor(pymysql.cursors.DictCursor)
            cursor.callproc('getEmpleados')
            rows = cursor.fetchall()
            return rows
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conexion.close()

    def getEmpleadoID(self):
        try:
            conexion = cx.conecta()
            cursor = conexion.cursor(pymysql.cursors.DictCursor)
            cursor.callproc('getEmpleadosID',[self.idempleado])
            rows=cursor.fetchall()
            return rows
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conexion.close()   

    def addEmpleados(self):
            usuario = self.usuario
            password = self.password
            idpersona = self.idpersona
            data = [usuario,password,idpersona]
            try:
                conexion=cx.conecta()
                cursor=conexion.cursor(pymysql.cursors.DictCursor)
                cursor.callproc("addEmpleados",data)
                conexion.commit()
                return 1
            except Exception as e:
                print(e)
            finally:
                cursor.close()
                conexion.close() 

    def updateEmpleados(self):
            try:
                idemp = self.idempleado
                usuario = self.usuario
                password = self.password
                data = [idemp,usuario,password]  
                conexion = cx.conecta()
                cursor=conexion.cursor(pymysql.cursors.DictCursor)
                cursor.callproc("updEmpleados",data)
                conexion.commit()
                return 1
            except Exception as e:
                print(e)
            finally:
                cursor.close()
                conexion.close()   

    def deleteEmpleado(self):
        try:
            conexion=cx.conecta()
            cursor=conexion.cursor(pymysql.cursors.DictCursor)
            cursor.callproc('deleteEmpleados',[self.idempleado])
            conexion.commit()
            return 1
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conexion.close()              
             