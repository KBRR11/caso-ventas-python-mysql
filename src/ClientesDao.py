import pymysql
from conexion import Conexion
from sqlalchemy import create_engine
from json import dumps
cx = Conexion()

class Cliente:
    idcliente = 0
    direccion = ""
    dni = ""
    idpersona = 0
    nombres = ""
    apellidos = ""

    def getAllClientes(self):
        try:
            conexion = cx.conecta()
            cursor = conexion.cursor(pymysql.cursors.DictCursor)
            cursor.callproc('getClientes')
            rows = cursor.fetchall()
            return rows
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conexion.close() 

    def getClienteID(self):
        try:
            conexion = cx.conecta()
            cursor = conexion.cursor(pymysql.cursors.DictCursor)
            cursor.callproc('getClientesID',[self.idcliente])
            rows=cursor.fetchall()
            return rows
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conexion.close() 

    def addClientes(self):
            direccion = self.direccion
            dni = self.dni
            idpersona = self.idpersona
            data = [direccion,dni,idpersona]
            try:
                conexion=cx.conecta()
                cursor=conexion.cursor(pymysql.cursors.DictCursor)
                cursor.callproc("addClientes",data)
                conexion.commit()
                return 1
            except Exception as e:
                print(e)
            finally:
                cursor.close()
                conexion.close() 

    def updateClientes(self):
        try:
            idcli = self.idcliente
            direccion = self.direccion
            dni = self.dni
            data = [idcli,direccion,dni]  
            conexion = cx.conecta()
            cursor=conexion.cursor(pymysql.cursors.DictCursor)
            cursor.callproc("updClientes",data)
            conexion.commit()
            return 1
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conexion.close()   

    def deleteCliente(self):
        try:
            conexion=cx.conecta()
            cursor=conexion.cursor(pymysql.cursors.DictCursor)
            cursor.callproc('deleteClientes',[self.idcliente])
            conexion.commit()
            return 1
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conexion.close()                                      